package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class KP_ActiveAndFitPage {
public static WebDriver driver = null;
	
	public KP_ActiveAndFitPage(WebDriver driver){
		KP_ActiveAndFitPage.driver=driver;
	}
	
	public WebElement Header(String environement) throws Exception {

		WebElement element = null;
		try {
			
			element = driver.findElement(By.tagName("header"));

		} catch (Exception e) {
			throw (e);
		}

		return element;
	}
	
	public WebElement MarqueeImageDesktop(String environement) throws Exception {

		WebElement element = null;
		try {

			if (environement.equals("BE"))
				element = driver.findElement(By.className("l-desktop"));
			else if (environement.equals("STAGING"))
				element = driver.findElement(By.className("l-desktop"));

		} catch (Exception e) {
			throw (e);
		}

		return element;
	}
	
	public WebElement MarqueeImageMobile(String environement) throws Exception {

		WebElement element = null;
		try {

			if (environement.equals("BE"))
				element = driver.findElement(By.className("l-mobile"));
			else if (environement.equals("STAGING"))
				element = driver.findElement(By.className("l-mobile"));

		} catch (Exception e) {
			throw (e);
		}

		return element;
	}
	
	public WebElement MarqueeOverlapText(String environement) throws Exception {

		WebElement element = null;
		try {

			if (environement.equals("BE"))
				element = driver.findElement(By.className("headline-holder"));
			else if (environement.equals("STAGING"))
				element = driver.findElement(By.className("headline-holder"));

		} catch (Exception e) {
			throw (e);
		}

		return element;
	}
	
	/*
	public WebElement MarqueeLearnMoreText(String environement) throws Exception {

		WebElement element = null;
		try {

			if (environement.equals("BE"))
				element = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]/div[2]/a"));
			else if (environement.equals("STAGING"))
				element = driver.findElement(By.xpath("//*[@id='maincontent']/div/div[1]/div[2]/span"));

		} catch (Exception e) {
			throw (e);
		}

		return element;
	}
	*/
	
	public List<WebElement> BreadcrumbNavigationItems(String environement) throws Exception {

		List<WebElement> elements = null;
		try {
			
			if (environement.equals("BE"))
				elements = driver.findElement(By.className("gb-bread-crumbs")).findElements(By.tagName("li"));
			else if (environement.equals("STAGING"))
				elements = driver.findElement(By.className("gb-bread-crumbs")).findElements(By.tagName("li"));

		} catch (Exception e) {
			throw (e);
		}

		return elements;
	}
	
	public WebElement GymIntroductionTitle(String environement) throws Exception {

		WebElement element = null;
		try {
			element = driver.findElement(By
					.id("kp-main"));

		} catch (Exception e) {
			throw (e);
		}

		return element;
	}
	
	public WebElement GymIntroductionSubTitle(String environement) throws Exception {

		WebElement element = null;
		try {

			if (environement.equals("BE"))
				element = driver.findElement(By.className("intro-section")).findElement(By.tagName("h2"));
			else if (environement.equals("STAGING"))
				element = driver.findElement(By.className("intro-section")).findElement(By.tagName("h2"));

		} catch (Exception e) {
			throw (e);
		}

		return element;
	}
	
	public WebElement GymIntroductionIntro(String environement) throws Exception {

		WebElement element = null;
		try {
			element = driver.findElement(By
					.className("intro"));

		} catch (Exception e) {
			throw (e);
		}

		return element;
	}
	
	public List<WebElement> GymSectionImages(String environement) throws Exception {

		List<WebElement> elements = null;
		try {
			elements = driver.findElement(By
					.className("kiwi-grid")).findElements(By.tagName("img"));

		} catch (Exception e) {
			throw (e);
		}

		return elements;
	}
	
	public List<WebElement> GymSectionTitles(String environement) throws Exception {

		List<WebElement> elements = null;
		try {
			elements = driver.findElement(By
					.className("kiwi-grid")).findElements(By.className("title"));

		} catch (Exception e) {
			throw (e);
		}

		return elements;
	}
	
	public List<WebElement> GymSectionSeperators(String environement) throws Exception {

		List<WebElement> elements = null;
		try {
			elements = driver.findElement(By
					.className("kiwi-grid")).findElements(By.className("seperator"));

		} catch (Exception e) {
			throw (e);
		}

		return elements;
	}
	
	public List<WebElement> GymSectionTexts(String environement) throws Exception {

		List<WebElement> elements = null;
		try {
			elements = driver.findElement(By
					.className("kiwi-grid")).findElements(By.className("text-holder"));

		} catch (Exception e) {
			throw (e);
		}

		return elements;
	}
	
	public List<WebElement> SerpentineImages(String environement) throws Exception {

		List<WebElement> elements = null;
		try {
			
			if (environement.equals("BE"))
				elements = driver.findElement(By.className("articles-section")).findElements(By.tagName("img"));
			else if (environement.equals("STAGING"))
				elements = driver.findElement(By.className("articles-section")).findElements(By.tagName("img"));

		} catch (Exception e) {
			throw (e);
		}

		return elements;
	}
	
	public List<WebElement> SerpentineTitles(String environement) throws Exception {

		List<WebElement> elements = null;
		try {

			if (environement.equals("BE"))
				elements = driver.findElement(By.className("articles-section")).findElements(By.className("title"));
			else if (environement.equals("STAGING"))
				elements = driver.findElement(By.className("articles-section")).findElements(By.className("title"));

		} catch (Exception e) {
			throw (e);
		}

		return elements;
	}
	
	public List<WebElement> SerpentineSeperators(String environement) throws Exception {

		List<WebElement> elements = null;
		try {
			
			if (environement.equals("BE"))
				elements = driver.findElement(By.className("articles-section")).findElements(By.className("seperator"));
			else if (environement.equals("STAGING"))
				elements = driver.findElement(By.className("articles-section")).findElements(By.className("seperator"));

		} catch (Exception e) {
			throw (e);
		}

		return elements;
	}
	
	public List<WebElement> SerpentineTexts(String environement) throws Exception {

		List<WebElement> elements = null;
		try {
			
			if (environement.equals("BE"))
				elements = driver.findElement(By.className("articles-section")).findElements(By.className("content"));
			else if (environement.equals("STAGING"))
				elements = driver.findElement(By.className("articles-section")).findElements(By.className("content"));

		} catch (Exception e) {
			throw (e);
		}

		return elements;
	}
	
	public WebElement SocialSharingTitle(String environement) throws Exception {

		WebElement element = null;
		try {

			if (environement.equals("BE"))
				element = driver.findElement(By.className("social-section")).findElement(By.tagName("h3"));
			else if (environement.equals("STAGING"))
				element = driver.findElement(By.className("social-section")).findElement(By.tagName("h3"));
			

		} catch (Exception e) {
			throw (e);
		}

		return element;
	}
	
	public WebElement SocialSharingFacebook(String environement) throws Exception {

		WebElement element = null;
		try {

			element = driver.findElement(By
					.className("facebook"));

		} catch (Exception e) {
			throw (e);
		}

		return element;
	}
	
	public WebElement SocialSharingTwitter(String environement) throws Exception {

		WebElement element = null;
		try {

			element = driver.findElement(By
					.className("twitter"));

		} catch (Exception e) {
			throw (e);
		}

		return element;
	}
	
	public WebElement SocialSharingInstagram(String environement) throws Exception {

		WebElement element = null;
		try {

			element = driver.findElement(By
					.className("instagram"));

		} catch (Exception e) {
			throw (e);
		}

		return element;
	}
	
	public WebElement FootNote(String environement) throws Exception {

		WebElement element = null;
		try {

			if (environement.equals("BE"))
				element = driver.findElement(By.className("note-section"));
			else if (environement.equals("STAGING"))
				element = driver.findElement(By.className("note-section"));

		} catch (Exception e) {
			throw (e);
		}

		return element;
	}
	
	public WebElement Footer(String environement) throws Exception {

		WebElement element = null;
		try {

			if (environement.equals("BE"))
				element = driver.findElement(By.className("v1"));
			else if (environement.equals("STAGING"))
				element = driver.findElement(By.className("v1"));
			

		} catch (Exception e) {
			throw (e);
		}

		return element;
	}
}
