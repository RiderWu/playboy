package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pageobject.KP_ActiveAndFitPage;
import com.web.Common;
import com.web.Constant;
//import com.utility.MailUtility;

public class KP_ActiveAndFitTestCases {

	public static String StartTime = null;
	public WebDriver driver = null;
	public static String CurrentEnvironment = null;
	public static String CurrentBrowser = null;
	public static String CurrentMarket = null;
	public static String CurrentWidth = null;
	public static String CurrentHeight = null;

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		StartTime = datetime.format(new Date());
		CurrentEnvironment = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(String browser, String market, String width, String height) throws Exception {
		CurrentBrowser = browser;
		CurrentMarket = market;
		CurrentWidth = width;
		CurrentHeight = height;
		driver = Common.openBrowser(CurrentBrowser, String.format(Constant.Get_KP_ActiveAndFitPage(CurrentEnvironment), market), width, height);
		
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void KP_ActiveAndFit_VerifyTheLayoutForHeader() throws Exception {
		int screenshotNumber = 1;
		KP_ActiveAndFitPage kafpage = new KP_ActiveAndFitPage(driver);
		try {

			Assert.assertTrue(kafpage.Header(CurrentEnvironment).isDisplayed(), "No Header");
			Common.takeScreenShot(driver, kafpage.Header(CurrentEnvironment), CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					screenshotNumber++);

		} catch (Exception e) {

			e.printStackTrace();
			Assert.assertTrue(false, e.getMessage());
			Common.takeScreenShot(driver, null, CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					0);
		}
	}

	@Test
	public void KP_ActiveAndFit_VerifyTheLocationAndStyleForMarqueeDesktop() throws Exception {
		int screenshotNumber = 1;
		try {

			KP_ActiveAndFitPage kafpage = new KP_ActiveAndFitPage(driver);

			Assert.assertTrue(kafpage.MarqueeImageDesktop(CurrentEnvironment).isDisplayed(), "No Marquee Image");
			Assert.assertTrue(kafpage.MarqueeOverlapText(CurrentEnvironment).isDisplayed(), "No Marquee Overlap Text");
			//Assert.assertTrue(kafpage.MarqueeLearnMoreText(CurrentEnvironment).isDisplayed(), "No Marquee Learn MoreText");
			Common.takeScreenShot(driver, kafpage.MarqueeImageDesktop(CurrentEnvironment), CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					screenshotNumber++);

		} catch (Exception e) {
			Common.takeScreenShot(driver, null, CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					0);
			e.printStackTrace();
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test
	public void KP_ActiveAndFit_VerifyTheLocationAndStyleForMarqueeMobile() throws Exception {
		int screenshotNumber = 1;
		try {

			KP_ActiveAndFitPage kafpage = new KP_ActiveAndFitPage(driver);

			Assert.assertTrue(kafpage.MarqueeImageMobile(CurrentEnvironment).isDisplayed(), "No Marquee Image");
			Assert.assertTrue(kafpage.MarqueeOverlapText(CurrentEnvironment).isDisplayed(), "No Marquee Overlap Text");
			//Assert.assertTrue(kafpage.MarqueeLearnMoreText(CurrentEnvironment).isDisplayed(), "No Marquee Learn MoreText");
			Common.takeScreenShot(driver, kafpage.MarqueeImageMobile(CurrentEnvironment), CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					screenshotNumber++);

		} catch (Exception e) {
			Common.takeScreenShot(driver, null, CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					0);
			e.printStackTrace();
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@Test
	public void KP_ActiveAndFit_VerifyTheLocationAndStyleForBreadcrumbNavigation() throws Exception {
		int screenshotNumber = 1;
		try {

			KP_ActiveAndFitPage kafpage = new KP_ActiveAndFitPage(driver);

			Common.scollTo(kafpage.BreadcrumbNavigationItems(CurrentEnvironment).get(0));
			Assert.assertTrue(kafpage.BreadcrumbNavigationItems(CurrentEnvironment).get(0).isDisplayed(),
					"No Breadcrumb Navigation Home");
			Assert.assertTrue(kafpage.BreadcrumbNavigationItems(CurrentEnvironment).get(1).isDisplayed(),
					"No Breadcrumb Navigation Hawaii");
			Assert.assertTrue(kafpage.BreadcrumbNavigationItems(CurrentEnvironment).get(2).isDisplayed(),
					"No Breadcrumb Navigation Free Gym Membership");
			Common.takeScreenShot(driver, kafpage.BreadcrumbNavigationItems(CurrentEnvironment).get(0), CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					screenshotNumber++);

		} catch (Exception e) {
			Common.takeScreenShot(driver, null, CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					0);
			e.printStackTrace();
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@Test
	public void KP_ActiveAndFit_VerifyTheLocationAndStyleForGymIntroduction() throws Exception {
		int screenshotNumber = 1;
		try {

			KP_ActiveAndFitPage kafpage = new KP_ActiveAndFitPage(driver);

			Common.scollTo(kafpage.GymIntroductionTitle(CurrentEnvironment));
			Assert.assertTrue(kafpage.GymIntroductionTitle(CurrentEnvironment).isDisplayed(), "No Gym Introduction Title");
			Assert.assertTrue(kafpage.GymIntroductionSubTitle(CurrentEnvironment).isDisplayed(), "No Gym Introduction SubTitle");
			Assert.assertTrue(kafpage.GymIntroductionIntro(CurrentEnvironment).isDisplayed(), "No Gym Introduction Intro");
			Common.takeScreenShot(driver, kafpage.GymIntroductionTitle(CurrentEnvironment), CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					screenshotNumber++);

		} catch (Exception e) {
			Common.takeScreenShot(driver, null, CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					0);
			e.printStackTrace();
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@Test
	public void KP_ActiveAndFit_VerifyTheLocationAndStyleForGymSection() throws Exception {
		int screenshotNumber = 1;
		try {

			KP_ActiveAndFitPage kafpage = new KP_ActiveAndFitPage(driver);

			Common.scollTo(kafpage.GymSectionImages(CurrentEnvironment).get(0));
			for (int i = 0; i < 3; i++) {
				Assert.assertTrue(kafpage.GymSectionImages(CurrentEnvironment).get(i).isDisplayed(),
						String.format("No Gym Section Images %d", i));
				Assert.assertTrue(kafpage.GymSectionTitles(CurrentEnvironment).get(i).isDisplayed(),
						String.format("No Gym Section Titles %d", i));
				Assert.assertTrue(kafpage.GymSectionSeperators(CurrentEnvironment).get(i).isDisplayed(),
						String.format("No Gym Section Seperator %d", i));
				Assert.assertTrue(kafpage.GymSectionTexts(CurrentEnvironment).get(i).isDisplayed(),
						String.format("No Gym Section Text %d", i));
			}
			Common.takeScreenShot(driver,kafpage.GymSectionImages(CurrentEnvironment).get(0), CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					screenshotNumber++);

		} catch (Exception e) {
			Common.takeScreenShot(driver, null, CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					0);
			e.printStackTrace();
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test
	public void KP_ActiveAndFit_VerifyTheLocationAndStyleForSerpentineDisplaySection() throws Exception {
		int screenshotNumber = 1;
		try {

			KP_ActiveAndFitPage kafpage = new KP_ActiveAndFitPage(driver);

			for (int i = 0; i < 4; i++) {
				Common.scollTo(kafpage.SerpentineImages(CurrentEnvironment).get(i));
				Assert.assertTrue(kafpage.SerpentineImages(CurrentEnvironment).get(i).isDisplayed(),
						String.format("No Serpentine Images %d", i));
				Assert.assertTrue(kafpage.SerpentineTitles(CurrentEnvironment).get(i).isDisplayed(),
						String.format("No Serpentine Titles %d", i));
				Assert.assertTrue(kafpage.SerpentineSeperators(CurrentEnvironment).get(i).isDisplayed(),
						String.format("No Serpentine Seperator %d", i));
				Assert.assertTrue(kafpage.SerpentineTexts(CurrentEnvironment).get(i).isDisplayed(),
						String.format("No Serpentine Text %d", i));
				Common.takeScreenShot(driver, kafpage.SerpentineImages(CurrentEnvironment).get(i), CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
						screenshotNumber+i);
			}

		} catch (Exception e) {
			Common.takeScreenShot(driver, null, CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					0);
			e.printStackTrace();
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test
	public void KP_ActiveAndFit_VerifyTheLocationAndStyleForSocialSharing() throws Exception {
		int screenshotNumber = 1;
		try {

			KP_ActiveAndFitPage kafpage = new KP_ActiveAndFitPage(driver);

			Common.scollTo(kafpage.SocialSharingTitle(CurrentEnvironment));
			Assert.assertTrue(kafpage.SocialSharingTitle(CurrentEnvironment).isDisplayed(), "No Social Sharing Title");
			Assert.assertTrue(kafpage.SocialSharingFacebook(CurrentEnvironment).isDisplayed(), "No Social Sharing Facebook");
			Assert.assertTrue(kafpage.SocialSharingTwitter(CurrentEnvironment).isDisplayed(), "No Social Sharing Twitter");
			Assert.assertTrue(kafpage.SocialSharingInstagram(CurrentEnvironment).isDisplayed(), "No Social Sharing Youtube");
			Common.takeScreenShot(driver, kafpage.SocialSharingTitle(CurrentEnvironment), CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					screenshotNumber++);

		} catch (Exception e) {
			Common.takeScreenShot(driver, null, CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					0);
			e.printStackTrace();
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test
	public void KP_ActiveAndFit_VerifyTheLocationAndStyleForFootNote() throws Exception {
		int screenshotNumber = 1;
		try {

			KP_ActiveAndFitPage kafpage = new KP_ActiveAndFitPage(driver);

			Common.scollTo(kafpage.FootNote(CurrentEnvironment));
			Assert.assertTrue(kafpage.FootNote(CurrentEnvironment).isDisplayed(), "No Foot Note");
			Common.takeScreenShot(driver, kafpage.FootNote(CurrentEnvironment), CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					screenshotNumber++);

		} catch (Exception e) {
			Common.takeScreenShot(driver, null, CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					0);
			e.printStackTrace();
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test
	public void KP_ActiveAndFit_VerifyTheLayoutForFooter() throws Exception {
		int screenshotNumber = 1;
		try {

			KP_ActiveAndFitPage kafpage = new KP_ActiveAndFitPage(driver);

			Common.scollTo(kafpage.Footer(CurrentEnvironment));
			Assert.assertTrue(kafpage.Footer(CurrentEnvironment).isDisplayed(), "No Footer");
			Common.takeScreenShot(driver, kafpage.Footer(CurrentEnvironment), CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					screenshotNumber++);

		} catch (Exception e) {
			Common.takeScreenShot(driver, null, CurrentBrowser+"_"+CurrentMarket+"_"+CurrentWidth+"_"+CurrentHeight, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(),
					0);
			e.printStackTrace();
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
