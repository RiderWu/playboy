package com.web;

public class Constant {
	
	//Millennium Hotels
	public static final String homepage = "https://www.millenniumhotels.com/%s";
	public static final String hotalspage = "https://www.millenniumhotels.com/%s/hotels/";
	
	//Kaiser Permanente Active & Fit
	public static String KP_ActiveAndFitPage = "http://profero:profero@kp-activefit.uidev.proferochina.com/activefit-index.html";
	
	public static String Get_KP_ActiveAndFitPage(String environemt) throws Exception{
		if(environemt.equals("FE")){
			KP_ActiveAndFitPage = "http://profero:profero@kp-activefit.uidev.proferochina.com/activefit-index.html";
		} else if(environemt.equals("BE")){
			KP_ActiveAndFitPage = "http://profero:profero@thrive.proferochina.com/care-near-hawaii/active-and-fit";
		} else if(environemt.equals("STAGING")){
			KP_ActiveAndFitPage = "http://preview.thrive.prod.dpaprod.kpwpce.kp-aws-cloud.org/care-near-hawaii/active-and-fit";
		}
		return KP_ActiveAndFitPage;
	}
}
