package com.web;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Common {

	public static WebDriver driver = null;
	public static String osName = System.getProperties().getProperty("os.name");
	//public static String osArch = System.getProperties().getProperty("os.arch");

	public static WebDriver openBrowser(String browserName, String url, String width, String height) throws Exception {
		try {

			if (browserName.equalsIgnoreCase("FireFox")) {
				ProfilesIni allProfiles = new ProfilesIni();
				FirefoxProfile profile = allProfiles.getProfile("default");
				driver = new FirefoxDriver(profile);
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
				driver.manage().window().setSize(new Dimension(Integer.parseInt(width),Integer.parseInt(height)));
				driver.get(url);

			} else if (browserName.equalsIgnoreCase("Chrome")) {
				if (osName.toLowerCase().contains("win")) {
					System.setProperty("webdriver.chrome.driver", "res\\chromedriver.exe");
				} else if (osName.toLowerCase().contains("mac")){
					System.setProperty("webdriver.chrome.driver", "res/mac/chromedriver");
				}
				driver = new ChromeDriver();
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
				driver.manage().window().setSize(new Dimension(Integer.parseInt(width),Integer.parseInt(height)));
				driver.get(url);

			} else if (browserName.equalsIgnoreCase("IE")) {
				DesiredCapabilities capability = new DesiredCapabilities();
				capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				capability.setCapability("ignoreProtectedModeSettings", true);
				System.setProperty("webdriver.ie.driver", "res\\IEDriverServer.exe");
				driver = new InternetExplorerDriver(capability);
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
				driver.manage().window().setSize(new Dimension(Integer.parseInt(width),Integer.parseInt(height)));
				driver.get(url);

			}

		} catch (TimeoutException ignored) {
			System.out.println("Browser load timeout.");
			((JavascriptExecutor) driver).executeScript("window.stop()");
			return driver;
		} catch (Exception e) {
			System.out.println("Browser setting error.");
			e.printStackTrace();
			driver.quit();
			return null;
		}

		return driver;
	}

	public static void wait(int timeout, String xpath) {
		(new WebDriverWait(driver, timeout)).until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver d) {
				return d.findElement(By.xpath(xpath));
			}
		});
	}

	public static void scollTo(WebElement element) {
		((JavascriptExecutor) driver)
				.executeScript(String.format("window.scrollTo(0, %d)", element.getLocation().getY() - 100));
	}

	public static void takeScreenShot(WebDriver driver, WebElement element, String foldername, String timeStamp, String testCaseName,
			int number) throws Exception {
		try {

			String path = "screenshots\\" + timeStamp + "\\" + foldername + "\\" + testCaseName + "_" + number + ".png";
			File file = new File(path);
			FileUtils.copyFile(((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE), file);
			
			int LocationY = 0;
			if(foldername.startsWith("Chrome"))LocationY = 0;
			else if(foldername.startsWith("FireFox")||foldername.startsWith("IE"))LocationY = element.getLocation().getY();
			Rectangle area = new Rectangle(0, LocationY, driver.manage().window().getSize().getWidth(), driver.manage().window().getSize().getHeight());
			cutImage(file,path,area);

		} catch (Exception e) {
			throw (e);
		}

	}
	
	public static void cutImage(File srcImg, String destImgPath, java.awt.Rectangle rect){
        File destImg = new File(destImgPath);
        if(destImg.exists()){
            String p = destImg.getPath();
            try {
                if(!destImg.isDirectory()) p = destImg.getParent();
                if(!p.endsWith(File.separator)) p = p + File.separator;
                cutImage(srcImg, new java.io.FileOutputStream(p + "ScreenShot_" + srcImg.getName()), rect);
            } catch (FileNotFoundException e) {
            	System.out.println("the dest image is not exist.");
            }
        }else System.out.println("the dest image folder is not exist.");
    }
	
	public static void cutImage(File srcImg, OutputStream output, java.awt.Rectangle rect){
        if(srcImg.exists()){
            java.io.FileInputStream fis = null;
            ImageInputStream iis = null;
            try {
                fis = new FileInputStream(srcImg);
                
                String types = Arrays.toString(ImageIO.getReaderFormatNames()).replace("]", ",");
                String suffix = null;
                
                if(srcImg.getName().indexOf(".") > -1) {
                    suffix = srcImg.getName().substring(srcImg.getName().lastIndexOf(".") + 1);
                }
                if(suffix == null || types.toLowerCase().indexOf(suffix.toLowerCase()+",") < 0){
                    System.out.println("Sorry, the image suffix is illegal. the standard image suffix is {}." + types);
                    return ;
                }
                
                iis = ImageIO.createImageInputStream(fis);
                
                ImageReader reader = ImageIO.getImageReadersBySuffix(suffix).next();
                reader.setInput(iis,true);
                ImageReadParam param = reader.getDefaultReadParam();
                param.setSourceRegion(rect);
                BufferedImage bi = reader.read(0, param);
                ImageIO.write(bi, suffix, output);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if(fis != null) fis.close();
                    if(iis != null) iis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }else {
            System.out.println("the src image is not exist.");
        }
    }

}
